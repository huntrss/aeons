# Async ExecutOr No Std

An experimental, fun project to create an async executor in no_std Rust.
It is derived from [whorl](https://github.com/mgattozzi/whorl).

## Caution

This crate uses unsafe and is unsafe and will trigger undefined behavior. See [Strange behavior: Writing to a pointer or not doesn't affect the outcome](https://users.rust-lang.org/t/strange-behavior-writing-to-a-pointer-or-not-doesnt-affect-the-outcome/105752) for more details.

**Don't use this crate!**
