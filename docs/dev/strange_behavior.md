# Strange behavior: Writing to a pointer or not doesn't affect the outcome

I have a very strange behavior in my hobby project, where it doesn't matter if I write to a pointer or not. I will try to explain my context below, I fear this is going to be a lengthy description.

## What am I building

I have created (or trying to create) an async executor, that doesn't use the standard library no the `alloc` create, i.e. I don't use dynamic allocation. Additionally I am not using macros to generate code during compile time to store the futures that are spawned unto the executor. This means I need a different way to store these futures, which I will describe below.

## Why?

Fun and learning. I learned a lot (or at least came across a lot of interesting topics) and I had most of the time fun doing so. Currently I have no intentions on doing something more with this project.

## Storing futures

In order for the executor to poll the futures, it needs to have a pinned pointer to it. The way I wanted to resolve this, is by handling a `& mut [u8]` slice to the executor and copying the futures that are spawned unto the executor into this slice. More or less, I allocate the space for the future, get a pointer to it the allocated space and write the future to the pointer.

You can find the allocator code here: [slot_allocator](https://gitlab.com/huntrss/aeons/-/blob/pointer_write/src/slot_allocator.rs).

Since `Future` is only a trait, I need to be able to handle trait objects (or only being able to store one particular type that implements `Future`). To do this I wrote some kind of helper, that is similar to `Box` but that works with the aforementioned slot allocator: [slot_box](https://gitlab.com/huntrss/aeons/-/blob/pointer_write/src/slot_box.rs). `SlotBox` does also provide a method to pin the future, which comes in handy for the executor, as you might imagine.

## The strange behavior

The strange thing is: If I write to the allocated pointer or not, does not matter:

* Here I write to the pointer: [pointer_write branch - SlotBox::new](https://gitlab.com/huntrss/aeons/-/blob/pointer_write/src/slot_box.rs#L23-25)
* In another branch I deleted these lines: [no_pointer_write branch - SlotBox::new](https://gitlab.com/huntrss/aeons/-/blob/no_pointer_write/src/slot_box.rs?ref_type=heads#L22)

My tests as well as my simple usage example behave exactly the same, no matter if I write to the pointer or not:

* Tests in pointer_write branch (tests are identical in both branches):
    * [Polling future](https://gitlab.com/huntrss/aeons/-/blob/no_pointer_write/src/slot_box.rs?ref_type=heads#L53)
    * [Storing futures in an array](https://gitlab.com/huntrss/aeons/-/blob/no_pointer_write/src/slot_box.rs?ref_type=heads#L87)
* [Simple example](https://gitlab.com/huntrss/aeons/-/blob/no_pointer_write/examples/simple.rs?ref_type=heads)

## The question

Has anyone any idea, what is happening? From my perspective the tests should panic, since I take a `Pin` on an array that is zeroed and then `poll` it. I am confused, to say the least. I hope someone of you knows what is wrong or can nudge my in a direction where to investigate further.

