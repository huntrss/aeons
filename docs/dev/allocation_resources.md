# Allocation resources

Document that collects links and other resources about the topic of memory allocation in Rust. It's context are the allocation into a fixed sized `u8` buffer slice necessary for all tasks.

* [Allocator designs](https://os.phil-opp.com/allocator-designs/)
* [Rust - implement custom allocator](https://www.techieindoor.com/rust-implement-custom-allocator/)
* [How to create a custom memory allocator in Rust](https://www.brochweb.com/blog/post/how-to-create-a-custom-memory-allocator-in-rust/)
* [Rustonomicon - Implementing Vec](https://doc.rust-lang.org/nomicon/vec/vec.html)
* [Rust - Allocator API](https://doc.rust-lang.org/core/alloc/trait.Allocator.html)
* [dynamic memory allocation](http://mysterious.computer/memory-allocators-in-rust/)
* [Rust allocator and memory management](https://www.youtube.com/watch?v=fpkjmE-56Gw)
