# Resources

A collection of some links and resources that may be interesting.

* [concurrent-queue](https://crates.io/crates/concurrent-queue)
* [portable-atomic](https://crates.io/crates/portable-atomic)
