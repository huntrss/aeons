# 7. Re-using task ids and slots.

Date: 2023-11-18

## Status

Draft

## Context

When the number of tasks are limited, as proposed in [ADR 0006 - avoiding concurrency issues](/docs/adrs/0006-avoiding-concurrency-issues.md) then we either need an option to re-use task slots and task ids or live with the consequences, that not only the number of tasks existing in parallel (which is okay for a `no_std` and no `alloc` set-up) but also that the number of tasks that can exist at all, is limited. In this ADR we want to propose a solution to enable re-using task slots and task id.

## Decision

Another data structure, something like a FIFO, could organize the available task ids and hence available task slots. This FIFO should be wait-free or at least lock-free as the queue required for scheduled or active task ids. This means as soon as a task is ready, its task id could be added to this available task id queue. A new spawned task could then claim one of the available task ids from this queue. The order how tasks ids are claimed and the data of the slots are re-used, or the other way round, tasks are ready, their existing slots set to a default value (if necessary) and the task id pushed into the available task id queue must be carefully realized to avoid concurrency issues. But this would enable that tasks can be spawned, become ready over time, and re-spawned during the runtime of the application. The only limiting factor would be number of tasks that can exist or be active at the same time, but, as mentioned above, this is exactly what is acceptable in this `no_std` and no `alloc` environment.

## Consequences

This could solve the issue of re-using tasks, but the actual logic for "freeing" a task slot, resp., making a task id available, and "allocating" a task slot, resp., claiming a task id, may be complicated and error-prone.
