# 2. No alloc aeons

Date: 2023-11-06

## Status

Accepted

## Context

The plan is to create an async executor (aeons) that is usable in bare metal (in Rust terms `no_std`). Additionally, the plan is to don't require `alloc` as well. This may require that the async executor (aeons) will require a raw buffer to store the `dyn Future`'s that are required. However, to achieve this, I need to migrate the existing solution step-by-step.

## Decision

Remove the necessity for crate `alloc`, step-by-step.

## Consequences

Implementing an async executor without `alloc` is hard and may require that the async executor will need to implement an allocator based on a raw buffer itself.
