# 3. Data structures required for managing tasks

Date: 2023-11-06

## Status

Accepted

## Context

In this ADR I document the data structures I need to manage the tasks.

## Decision

* Future heap: A raw (`u8`) buffer, that is used to store the `dyn Future`'s. I currently assume that there is no other possibility than needing to dynamically allocate memory for user provided futures.
* Existing tasks: Basically an array of `Box<dyn Future>` or `Pin<Box<dyn Future>`. Due to the missing of `alloc`, this means that I need to provide an implementation of `Box` that works on the future's heap. This array is allocated on the stack and its size needs to be defined during compile time obviously. It defines the maximum available tasks, that can exist at the same time, besides the fact that the future's heap may run out-of-memory.
* Active tasks: A FIFO on the stack, that provides `pop` and `push_back`. This helps to organize all tasks/futures that can make progress, i.e., where it makes sense to poll then and don't receive an immediate `Poll::Pending`. This FIFO basically just has an indices into the existing tasks array.
* Sleeping / pending tasks: An array that provides `Option`'s of an index into the existing tasks, probably together with a generational index. The concept is, that wakers use indices of this array to wake-up pending tasks, i.e., these tasks are the scheduled to be executed by the async executor. This means the index of the task into the existing tasks array is pushed into the active tasks FIFO. The generational index may be necessary to avoid that cloned wakers do awake a task more than once. However I believe that a generational index will not avoid that this will happen, but it will reduce the likelihood of this happening at least in a short period of time.  

## Consequences

It may be the case that I missed some necessary structures or that they need to be refined. It is also possible that a data structure is not necessary. In any case, an update of this ADR or a new ADR will fix any conceptional error I may have made with this ADR.
