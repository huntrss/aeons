# 4. Waking

Date: 2023-11-15

## Status

Draft

## Context

If we want to have an async executor with `no_std` and without allocation, we need to have a useful design for wakers and waking in particular. The following challenges arise in this environment:

* We cannot use the [Wake](https://doc.rust-lang.org/std/task/trait.Wake.html) trait, since it is in `std` and not in core.
* We therefore need to create a [RawWaker](https://doc.rust-lang.org/core/task/struct.RawWaker.html) with its [RawWakerVTable](https://doc.rust-lang.org/core/task/struct.RawWakerVTable.html) that use a `* ()` (Rust's `void` pointer type) parameter named `data`. We therefore need a memory address to point to, that is not in the heap and we need to ensure that is not moved.
* In this interesting [discussion](https://users.rust-lang.org/t/waker-with-data/101140) on the Rust user forum it was proposed to use an index as the `data` parameter of the `RawWakerVTable`. This idea is still interesting but it assumes that there is a global variable that holds the async executor. Although, this is a valid solution, we prefer a solution where this decision (defining the async executor as a global variable) is up to the user. Therefore we cannot just use an index as the `data` pointer.

In this ADR, we define a solution of how we want to wake futures in our async executor, without need allocation or the executor to be a global variable.

## Decision

* First of all, we require the async executor to be pinned when executing `run` (or `block_on` if we rename this method). This particular method does execute all spawned tasks (or the "main" task, dependent on how and if we change it). By pinning the async executor we can create a pinned pointer that can be used in a waker structure, so that each waker can use the executor to re-schedule its sleeping task.
* We need to also have a pinned pointer to the waker, when we create the `RawWaker`. The plan is to use an array of wakers, the array itself pinned again so that on creating a waker, we use an inactive slot of this array to create our new waker, and use the address to this slot as the address required for the waker.
* A waker itself requires the following members:
    * A mut pointer to the async executor, it will be used to actually operate on the waker w.r.t. the async executor
    * An index (or task id) representing the task, the waker wants to wake
    * Most likely a generational index, which is checked to avoid double waking a task, or waking the wrong task
    * If necessary another member that indicates if the waker is already in use or free to be claimed as new slot for a new waker

## Consequences

* Pinning the async executor (or requiring it to be pinned) does complicate the `spawn` method which we want to provide in a way, that it can also be called inside an async function that is defined as a closure in the "proximity" of the created async executor.
