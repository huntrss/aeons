# 5. Creating an executor

Date: 2023-11-15

## Status

Draft

## Context

Although the async executor does not allocate any memory, it still needs to to use memory, e.g. a queue of active tasks. The required memory could be allocated on the stack (or some global memory section) whenever an async executor is created. However it may not be desirable to allocate the required memory on the stack for some embedded systems (e.g. the stack is too small). We therefore propose a solution how an async executor is created (and validated) to enable users flexibility on where the required memory is actually allocated.

## Decision

* The `create` method of the async executor will require mutable slices to all required data structures. In this way, these slices can be defined wherever the user wants it: Be it on the stack or specific memory sections.
* There will be helper functions that help to create the arrays for a specific size, as `const` functions (if this is always possible). Especially since some data structures do require specific empty of default values that may be more involved.
* In order to validate, that all slices are created with the correct size, there may be a `const` validation function that checks that the size of each slice matches too each other, or the creation method may return an error is it is insufficient somehow.
* Maybe further utility methods do help creating all arrays required at once or create another struct that from mutable references to these arrays to simplify the creation.
* Further utility methods (or the existing ones) may also help regarding the pinning of some of the required slices (not all necessarily require pinning).

## Consequences

* This will leak implementation details to the user
* It will also grant the user maximum flexibility to allocate the memory wherever it is desired
* Since all slices need to be mutable, there may be the risk, that these slices are also used without using the (hopefully) safe abstractions from the async executor
