# 6. Avoiding concurrency issues.

Date: 2023-11-15

## Status

Draft

## Context

Although the async executor is in particular designed to be single threaded, concurrency may still occur due to interrupts (or OS signal handlers). In the following situations there will be concurrent access to the async executor and its data:

* Whenever a waker is called: In an interrupt setting, this will obviously be concurrent to the execution of the `run` (or `block_on` method).
* We want to enable spawning further tasks from with a closure handed to the `spawn` method (and in proximity of the async executor variable).

We cannot use spin locks or similar techniques, since the nature of interrupts may lead to dead locks, if a less prior interrupt claimed the lock. Using critical sections or disabling interrupts for the problematic sections is also possible, but we have currently no idea how to abstract this in a way, that the async executor can be used in bare metal as well as OS situations.

In this ADR we define a solution, even if only temporarily, to address the issue of concurrency issues.

## Decision

* As a temporary solution, we only only a fixed number of tasks at all
    * Maybe we find a solution for re-using task slots
    * However, this would enable to let each task, its wakers etc. only operate on a specific index of (most) of the slices the async executor requires.
    * This is not straightforward to implement in Rust using safe APIs (or creating safe abstractions) it should be safe, if each task does only change the content of its slot and if there is no other task interfering with this slot at any moment in time.
* As another temporary solution, we not only allow a fixed number of tasks, but also only a fixed size for each future. This means a user defines the maximum memory required by each future (or task). In this way, we can also safely operate on the slices of raw memory to store the future's and their current state. This will make the API less ergonomic, since one may get (and should get) a compile error when the maximum size of memory for a task (resp. a future that is spawned) is exceeded. However, it should be possible to find the correct size through trial and error.
* There is one data structure (at least), that cannot be realized in a way, that each task only operates on its slot, and this is the active tasks queue. There needs to be a possibility to push a task to the tail of the queue (e.g., through the waker) as well as popping a task from the queue (during each loop iteration of the queue). For this we may require a lock-free or better wait-free queue. Hopefully we can find at [liblfds](https://liblfds.org/).

## Consequences

* If we find no solution to safely reuse task slots, then we have a very strong limitation
* If we find no solution to only allocate (and reuse) the used memory there will be an overhead of unused but allocated memory
* Lock-free or wait-free data structures require atomic operations that may not be available on all hardware, and therefore limiting the reach of this library
