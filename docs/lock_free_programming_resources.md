# Lock free programming resources

* [Introduction to lock-free programming](https://www.youtube.com/watch?app=desktop&v=RWCadBJ6wTk)
* [An introduction to lock-free programming](https://preshing.com/20120612/an-introduction-to-lock-free-programming/)
* [Lock and wait free methods presentation](https://www.youtube.com/watch?app=desktop&v=tbk7Fkrn_mg)
