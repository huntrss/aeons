use core::sync::atomic::{AtomicUsize, Ordering};

use crate::errors::AeonsErrors;

pub(crate) struct LFStack<'a, T> {
    elements: &'a mut [Option<T>],
    head_index: AtomicUsize,
}

impl<'a, T> LFStack<'a, T> {
    pub(crate) fn new(elements: &'a mut [Option<T>]) -> Self {
        Self {
            elements,
            head_index: AtomicUsize::new(0),
        }
    }
    pub(crate) fn push_back(&mut self, element: T) -> Result<(), AeonsErrors> {
        let mut old_head_index = self.head_index.load(Ordering::Relaxed);
        // when head_index "points" at the first element outside of the element slice
        // then the stack is full.
        if old_head_index == self.elements.len() {
            return Err(AeonsErrors::MaxNumberTasksExceeded);
        }
        if old_head_index > self.elements.len() {
            return Err(AeonsErrors::LockFreeStackPushError);
        }
        loop {
            let new_head_index = old_head_index + 1;
            // old head index need to be updated when the new head index was taken by another
            // thread / interrupt / signal handler
            old_head_index = match self.head_index.compare_exchange(
                old_head_index,
                new_head_index,
                Ordering::Relaxed,
                Ordering::Relaxed,
            ) {
                Err(value) => value,
                Ok(index) => {
                    if index >= self.elements.len() {
                        return Err(AeonsErrors::MaxNumberTasksExceeded);
                    }
                    self.elements[index] = Some(element);
                    return Ok(());
                }
            };
        }
    }

    // TODO: Return a Result instead?
    pub(crate) fn pop_front(&mut self) -> Option<T> {
        let mut old_head_index = self.head_index.load(Ordering::Relaxed);
        if old_head_index == 0 {
            // stack is empty
            return None;
        }
        if old_head_index > self.elements.len() {
            // actually an internal error, but I don't want to complicate the interface here
            return None;
        }
        loop {
            // move the head index to point to element before the top element
            let new_head_index = old_head_index - 1;
            old_head_index = match self.head_index.compare_exchange(
                old_head_index,
                new_head_index,
                Ordering::Relaxed,
                Ordering::Relaxed,
            ) {
                Err(value) => value,
                Ok(index) => {
                    // the old head index points to the currently inserted element
                    return self.elements[index - 1].take();
                }
            };
            if old_head_index == 0 {
                // the stack is now empty.
                // Another thread, interrupt or signal handler emptied it by popping the last
                // element.
                return None;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::AeonsErrors;

    #[test]
    fn simple_push() {
        // Arrange
        let mut elements: [Option<usize>; 1] = [None];
        let mut lfstack = LFStack::new(&mut elements);
        // Act
        let result = lfstack.push_back(1);
        // Assert
        assert!(result.is_ok());
        assert_eq!(Some(1), elements[0]);
    }

    #[test]
    fn simple_pop() {
        // Arrange
        let mut elements: [Option<usize>; 1] = [None];
        let mut lfstack = LFStack::new(&mut elements);
        // Act
        let option = lfstack.pop_front();
        assert!(option.is_none());
        let result = lfstack.push_back(12);
        assert!(result.is_ok());
        let option = lfstack.pop_front();
        // Assert
        assert_eq!(Some(12), option);
    }

    #[test]
    fn multiple_pushs_and_pops() {
        // Arrange
        let mut elements: [Option<usize>; 5] = [None; 5];
        let mut lfstack = LFStack::new(&mut elements);
        // Act
        assert!(lfstack.push_back(1).is_ok());
        assert!(lfstack.push_back(2).is_ok());
        assert!(lfstack.push_back(3).is_ok());
        assert_eq!(Some(3), lfstack.pop_front());
        assert!(lfstack.push_back(4).is_ok());
        assert!(lfstack.push_back(5).is_ok());
        assert_eq!(Some(5), lfstack.pop_front());
        assert!(lfstack.push_back(6).is_ok());
        assert!(lfstack.push_back(7).is_ok());
        assert_eq!(Some(7), lfstack.pop_front());
        assert!(lfstack.push_back(8).is_ok());
        assert_eq!(
            Err(AeonsErrors::MaxNumberTasksExceeded),
            lfstack.push_back(9)
        );
        // Assert
        assert_eq!(
            [Some(1), Some(2), Some(4), Some(6), Some(8)],
            lfstack.elements
        );
        assert_eq!(5, lfstack.head_index.load(Ordering::Relaxed));
        // Act again
        assert_eq!(Some(8), lfstack.pop_front());
        assert_eq!(Some(6), lfstack.pop_front());
        assert_eq!(Some(4), lfstack.pop_front());
        assert_eq!(Some(2), lfstack.pop_front());
        assert_eq!(Some(1), lfstack.pop_front());
        assert_eq!(None, lfstack.pop_front());
        // Assert again
        assert_eq!(0, lfstack.head_index.load(Ordering::Relaxed));
    }
}
