#![cfg_attr(not(test), no_std)]
extern crate alloc;

pub mod errors;
mod lfqueue;
mod lfstack;
mod slot_allocator;
mod slot_box;

use core::cell::{Cell, RefCell};
use core::future::Future;
use core::pin::Pin;
use core::ptr::drop_in_place;
use core::task::{Context, Poll, RawWaker, RawWakerVTable, Waker};
use errors::AeonsErrors;
use lfqueue::LFQueue;
use lfstack::LFStack;
use slot_allocator::SlotAllocator;
use slot_box::SlotBox;

type TaskId = usize;

pub type Task<'a> = Pin<&'a mut dyn Future<Output = ()>>;
type ActiveTasks<'a> = RefCell<LFQueue<'a, TaskId>>;
type AvailableSlots<'a> = RefCell<LFStack<'a, TaskId>>;

pub struct Executor<'a> {
    active_tasks: ActiveTasks<'a>,
    available_slots: AvailableSlots<'a>,
    slot_allocator: SlotAllocator<'a>,
    futures: &'a mut [Option<Task<'a>>],
    completed_tasks: Cell<u32>,
    wakers: Pin<&'a mut [TaskWaker<'a>]>,
}

impl<'a> Executor<'a> {
    pub fn new(
        maximum_number_of_tasks: usize,
        active_tasks: &'a mut [Option<TaskId>],
        available_slots: &'a mut [Option<TaskId>],
        maximum_memory_size_per_task: usize,
        memory: Pin<&'a mut [u8]>,
        futures: &'a mut [Option<Task<'a>>],
        wakers: Pin<&'a mut [TaskWaker<'a>]>,
    ) -> Result<Self, AeonsErrors> {
        let slot_allocator = SlotAllocator::new(
            memory,
            maximum_number_of_tasks,
            maximum_memory_size_per_task,
        )?;
        let active_tasks = RefCell::new(LFQueue::new(active_tasks));
        let available_slots = RefCell::new(LFStack::new(available_slots));
        for task_id in 0..maximum_number_of_tasks {
            available_slots.borrow_mut().push_back(task_id)?;
        }
        Ok(Self {
            active_tasks,
            available_slots,
            slot_allocator,
            futures,
            completed_tasks: Cell::new(0),
            wakers,
        })
    }

    pub fn spawn(&mut self, future: impl Future<Output = ()> + 'a) -> Result<(), AeonsErrors> {
        let task_id = match self.available_slots.borrow_mut().pop_front() {
            Some(id) => id,
            None => return Err(AeonsErrors::MaxNumberTasksExceeded),
        };
        let boxed_future = match SlotBox::new(future, &mut self.slot_allocator, task_id) {
            Ok(boxed_future) => boxed_future,
            Err(error) => {
                self.available_slots.borrow_mut().push_back(task_id)?;
                return Err(error);
            }
        };
        let pinned_future = boxed_future.pin();
        self.futures[task_id] = Some(pinned_future);
        self.active_tasks.borrow_mut().push_back(task_id)?;
        Ok(())
    }

    pub fn run(mut self: Pin<&mut Self>) -> Result<(), AeonsErrors> {
        let executor_ptr = self.as_mut().get_mut() as *mut Executor<'a>;
        loop {
            let task_id = match self.active_tasks.borrow_mut().pop_front() {
                Some(task_id) => task_id,
                None => return Ok(()),
            };

            let task_waker_ptr: *mut TaskWaker<'a> = self.wakers[task_id..].as_mut_ptr();
            TaskWaker::init(task_waker_ptr, executor_ptr, task_id);
            let waker = TaskWaker::into_waker(task_waker_ptr);
            let mut context = Context::from_waker(&waker);

            let mut completed_tasks: u32 = self.completed_tasks.get();
            if let Some(pinned_future) = &mut self.futures[task_id] {
                let future_state = pinned_future.as_mut().poll(&mut context);
                if future_state != Poll::Pending {
                    completed_tasks += 1;
                    unsafe { drop_in_place(pinned_future) };
                    // task is finished, clear its slot.
                    self.slot_allocator.zero(task_id)?;
                    // make slot available again for new tasks
                    self.available_slots.borrow_mut().push_back(task_id)?;
                }
            }
            self.completed_tasks.set(completed_tasks);
        }
    }

    fn reschedule_task(&mut self, task_id: TaskId) {
        let result = self.active_tasks.borrow_mut().push_back(task_id);
        if result.is_err() {
            let error_code = result.unwrap_err() as u32;
            panic!(
                "Error during rescheduling task, this means when waking up a task {error_code}."
            );
        };
    }

    pub fn completed_tasks(&self) -> u32 {
        self.completed_tasks.get()
    }
}

const VTABLE: RawWakerVTable = RawWakerVTable::new(clone_raw_waker, wake, wake_by_ref, drop);

#[derive(Clone, Copy)] // TODO: remove later
pub struct TaskWaker<'a> {
    task_id: Option<TaskId>,
    executor_ptr: Option<*mut Executor<'a>>,
}

pub const fn default_wakers<'a, const N: usize>() -> [TaskWaker<'a>; N] {
    [TaskWaker::default(); N]
}

impl<'a> TaskWaker<'a> {
    fn wake(self) {
        if let Some(task_id) = self.task_id {
            if let Some(executor_ptr) = self.executor_ptr {
                let executor_ref: &mut Executor<'a> = unsafe { &mut *executor_ptr };
                executor_ref.reschedule_task(task_id);
            }
        }
    }

    fn wake_by_ref(&self) {
        if let Some(task_id) = self.task_id {
            if let Some(executor_ptr) = self.executor_ptr {
                let executor_ref: &mut Executor<'a> = unsafe { &mut *executor_ptr };
                executor_ref.reschedule_task(task_id);
            }
        }
    }

    const fn default() -> Self {
        Self {
            task_id: None,
            executor_ptr: None,
        }
    }

    fn into_waker(ptr: *const TaskWaker<'a>) -> Waker {
        let raw_waker = RawWaker::new(ptr as *const (), &VTABLE);
        unsafe { Waker::from_raw(raw_waker) }
    }

    fn init(task_waker_ptr: *mut TaskWaker<'a>, executor_ptr: *mut Executor<'a>, task_id: usize) {
        unsafe { (*task_waker_ptr).executor_ptr = Some(executor_ptr) };
        unsafe { (*task_waker_ptr).task_id = Some(task_id) };
    }
}

unsafe fn clone_raw_waker(waker_ptr: *const ()) -> RawWaker {
    // Just the pointer is cloned resp. copied.
    // The actual data are part element in the array.
    // Therefore it is just necessary to copy the pointer when cloning.
    // Double drop will be avoided in the drop implementation itself.
    RawWaker::new(waker_ptr, &VTABLE)
}

unsafe fn wake(waker_ptr: *const ()) {
    let task_waker: TaskWaker = *(waker_ptr as *const TaskWaker);
    task_waker.wake();
}

unsafe fn wake_by_ref(waker_ptr: *const ()) {
    let task_waker_ref: &TaskWaker = &*(waker_ptr as *const TaskWaker);
    task_waker_ref.wake_by_ref();
}

unsafe fn drop(waker_ptr: *const ()) {
    let task_waker_ptr = waker_ptr as *mut TaskWaker;
    let optional_task_id = (*task_waker_ptr).task_id;
    // only "drop" the waker once, i.e. if the waker was already dropped, the waker's data will not be changed.
    if optional_task_id.is_some() {
        (*task_waker_ptr).task_id = None;
        (*task_waker_ptr).executor_ptr = None;
    }
}

#[cfg(test)]
mod tests {}
