use crate::errors::AeonsErrors;
use core::{pin::Pin, ptr::NonNull};

pub(crate) struct SlotAllocator<'a> {
    memory: Pin<&'a mut [u8]>,
    max_memory_size_slot: usize,
    max_slots: usize,
    // add bitfield or another slice to keep state if a slot is already occupied.
    // Or use another byte on each slot to keep the state if the slot is occupied.
    // This would spatially divide the state keeping of each slot from each other.
    // In this way accessing different slots can happen concurrently without concurrency issues.
}

impl<'a> SlotAllocator<'a> {
    pub(crate) fn new(
        memory: Pin<&'a mut [u8]>,
        max_slots: usize,
        max_memory_size_slot: usize,
    ) -> Result<Self, AeonsErrors> {
        let required_size = max_slots * max_memory_size_slot;
        if required_size > memory.len() {
            return Err(AeonsErrors::MemoryTooSmall);
        }
        Ok(Self {
            memory,
            max_memory_size_slot,
            max_slots,
        })
    }

    pub(crate) fn allocate(
        &mut self,
        slot_index: usize,
        size: usize,
        alignment: usize,
    ) -> Result<NonNull<[u8]>, AeonsErrors> {
        if slot_index >= self.max_slots {
            return Err(AeonsErrors::SlotIndexTooLarge);
        }

        let start_address = self.max_memory_size_slot * slot_index;
        let max_end_address_allowed = start_address + self.max_memory_size_slot;
        let start_address = align_up(start_address, alignment);
        let end_address = start_address + size;
        if end_address > max_end_address_allowed {
            return Err(AeonsErrors::SlotSizeTooSmall);
        }
        let pointer = NonNull::from(&mut self.memory[start_address..end_address]);
        Ok(pointer)
    }

    pub(crate) fn zero(&mut self, slot_index: usize) -> Result<(), AeonsErrors> {
        if slot_index >= self.max_memory_size_slot {
            return Err(AeonsErrors::SlotIndexTooLarge);
        }
        let start_index = self.max_memory_size_slot * slot_index;
        let end_index = self.max_memory_size_slot * (slot_index + 1);
        self.memory[start_index..end_index].fill(0);
        Ok(())
    }
}

// Aligns the start_address up based on the alignment
// Implementation copied from <https://os.phil-opp.com/allocator-designs/
fn align_up(addr: usize, align: usize) -> usize {
    let remainder = addr % align;
    if remainder == 0 {
        addr // addr already aligned
    } else {
        addr - remainder + align
    }
}
