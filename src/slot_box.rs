use core::mem::{align_of, size_of};
use core::ptr;
use core::{pin::Pin, ptr::NonNull};

use crate::errors::AeonsErrors;
use crate::slot_allocator::SlotAllocator;

pub struct SlotBox<T: ?Sized> {
    pointer: NonNull<T>,
}

impl<T> SlotBox<T> {
    pub(crate) fn new(
        value: T,
        slot_allocator: &mut SlotAllocator,
        slot_index: usize,
    ) -> Result<Self, AeonsErrors> {
        let raw_ptr = slot_allocator.allocate(slot_index, size_of::<T>(), align_of::<T>())?;
        let pointer = NonNull::new(raw_ptr.as_ptr() as *mut T);
        if pointer.is_none() {
            return Err(AeonsErrors::PointerIsNull);
        }
        let pointer = pointer.unwrap();
        unsafe {
            ptr::write(pointer.as_ptr(), value);
        }
        Ok(Self { pointer })
    }
    pub(crate) fn pin<'a>(mut self) -> Pin<&'a mut T> {
        unsafe { Pin::new_unchecked(self.pointer.as_mut()) }
    }
}

#[cfg(test)]
mod tests {
    use core::{
        future::Future,
        task::{Poll, Waker},
    };

    use alloc::sync::Arc;

    use super::*;

    struct NoopWaker;

    impl std::task::Wake for NoopWaker {
        fn wake(self: alloc::sync::Arc<Self>) {}
    }

    async fn test_future() -> u32 {
        let v = async { 3 }.await;
        11 + v
    }

    #[test]
    fn polling_slot_boxed_future() {
        // Arrange
        let memory = [0u8; 512];
        let memory = core::pin::pin!(memory);
        let slot_allocator = SlotAllocator::new(memory, 1, 512);
        let mut slot_allocator = match slot_allocator {
            Ok(v) => v,
            Err(_) => {
                assert!(false, "Creation of slot allocator failed.");
                return;
            }
        };
        let fut = test_future();
        let result = SlotBox::new(fut, &mut slot_allocator, 0);
        assert!(result.is_ok());
        let slot_box = match result {
            Ok(v) => v,
            Err(_) => {
                assert!(false, "Creation of slot box failed");
                return;
            }
        };
        // Act
        let future: Pin<&mut dyn Future<Output = u32>> = slot_box.pin();

        let waker = Arc::new(NoopWaker);
        let waker = Waker::from(waker);
        let mut context = core::task::Context::from_waker(&waker);
        let poll_result = future.poll(&mut context);

        // Assert
        assert_eq!(Poll::Ready(14), poll_result);
    }
    #[test]
    fn storing_pinned_futures_in_an_array() {
        // Arrange
        let memory = [0u8; 512];
        let memory = core::pin::pin!(memory);
        let slot_allocator = SlotAllocator::new(memory, 2, 256);
        let mut slot_allocator = match slot_allocator {
            Ok(v) => v,
            Err(_) => {
                assert!(false, "Creation of slot allocator failed.");
                return;
            }
        };

        let fut = test_future();
        let result = SlotBox::new(fut, &mut slot_allocator, 0);
        assert!(result.is_ok());
        let slot_box = match result {
            Ok(v) => v,
            Err(_) => {
                assert!(false, "Creation of slot box failed");
                return;
            }
        };
        let future: Pin<&mut dyn Future<Output = u32>> = slot_box.pin();
        let fut_2 = async { 2 };
        let slot_box_2 = SlotBox::new(fut_2, &mut slot_allocator, 1);
        assert!(slot_box_2.is_ok());
        let slot_box_2 = match slot_box_2 {
            Ok(v) => v,
            Err(_) => {
                assert!(false, "Creation of second slot box failed");
                return;
            }
        };
        let future_2: Pin<&mut dyn Future<Output = u32>> = slot_box_2.pin();
        let mut pinned_futures: [Option<Pin<&mut dyn Future<Output = u32>>>; 2] = [None, None];

        pinned_futures[0] = Some(future);
        pinned_futures[1] = Some(future_2);

        let waker = Arc::new(NoopWaker);
        let waker = Waker::from(waker);
        let mut context = core::task::Context::from_waker(&waker);
        // Act
        let poll_result_1 = pinned_futures[0]
            .as_mut()
            .expect("Just added future 1 must be avialable")
            .as_mut()
            .poll(&mut context);
        let poll_result_2 = pinned_futures[1]
            .as_mut()
            .expect("Just added future 2 must be available")
            .as_mut()
            .poll(&mut context);
        // Assert
        assert_eq!(Poll::Ready(14), poll_result_1);
        assert_eq!(Poll::Ready(2), poll_result_2);
    }
}
