use core::{
    mem::size_of,
    sync::atomic::{AtomicUsize, Ordering},
};

use crate::errors::AeonsErrors;

pub(crate) struct LFQueue<'a, T> {
    elements: &'a mut [Option<T>],
    head_length: AtomicUsize,
}

struct HeadAndLength {
    head: usize,
    length: usize,
}

impl HeadAndLength {
    fn from(head_length: usize) -> Self {
        // half of the size in bits.
        // size is in bytes, this means to get the bit size one must multiply with 8 or right shift by 3.
        // Halfing would be dividing by 2 or left shifting by 1.
        let halfed_size_bits = half_size_in_bits();
        let head = head_length >> halfed_size_bits;
        // To get the length, shift out head (the upper part of the bits)
        // and then shift back.
        let length = head_length << halfed_size_bits;
        let length = length >> halfed_size_bits;
        Self { head, length }
    }

    fn combine(&self) -> usize {
        let half_size_in_bits = half_size_in_bits();
        let head_length = self.head << half_size_in_bits;
        
        head_length | self.length
    }
}

fn half_size_in_bits() -> usize {
    size_of::<usize>() << 2
}

impl<'a, T> LFQueue<'a, T> {
    pub(crate) fn new(elements: &'a mut [Option<T>]) -> Self {
        Self {
            elements,
            head_length: AtomicUsize::new(0),
        }
    }

    pub(crate) fn push_back(&mut self, element: T) -> Result<(), AeonsErrors> {
        let mut old_head_length = self.head_length.load(Ordering::Relaxed);
        let mut head_and_length = HeadAndLength::from(old_head_length);
        let max_number_of_elements = self.elements.len();
        if head_and_length.length >= max_number_of_elements {
            return Err(AeonsErrors::MaxNumberOfActiveTasksExceeded);
        }

        loop {
            if head_and_length.length >= max_number_of_elements {
                return Err(AeonsErrors::MaxNumberOfActiveTasksExceeded);
            }
            // update length in order to get the next free slot
            head_and_length.length += 1;
            let new_head_length = head_and_length.combine();
            old_head_length = match self.head_length.compare_exchange(
                old_head_length,
                new_head_length,
                Ordering::Relaxed,
                Ordering::Relaxed,
            ) {
                Err(value) => value,
                Ok(head_length) => {
                    head_and_length = HeadAndLength::from(head_length);
                    if head_and_length.length >= max_number_of_elements {
                        return Err(AeonsErrors::MaxNumberOfActiveTasksExceeded);
                    }
                    // increasing the length actually reserves the slot
                    // before the new length
                    // TODO: consider wrapping here
                    let index =
                        (head_and_length.head + head_and_length.length) % max_number_of_elements;
                    self.elements[index] = Some(element);
                    return Ok(());
                }
            };
            // update head and length to the values that were changed
            // by some other thread, interrupt, signal
            head_and_length = HeadAndLength::from(old_head_length);
        }
    }

    pub(crate) fn pop_front(&mut self) -> Option<T> {
        let mut old_head_and_length = self.head_length.load(Ordering::Relaxed);
        let mut head_and_length = HeadAndLength::from(old_head_and_length);
        let max_number_of_elements = self.elements.len();
        loop {
            if head_and_length.length == 0 {
                return None;
            }
            // Head is updated by one and wrapped around
            head_and_length.head += 1;
            head_and_length.head %= max_number_of_elements;
            // length is incremented by one, but only of larger than 0. Otherwise there
            // are no elements left.
            head_and_length.length -= 1;
            let new_head_length = head_and_length.combine();
            old_head_and_length = match self.head_length.compare_exchange(
                old_head_and_length,
                new_head_length,
                Ordering::Relaxed,
                Ordering::Relaxed,
            ) {
                Err(current_head_length) => current_head_length,
                Ok(current_head_length) => {
                    let head_and_length = HeadAndLength::from(current_head_length);
                    // the current head is now the index that contains the
                    // element that will be popped.
                    let index = head_and_length.head;
                    return self.elements[index].take();
                }
            };
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_extract_head_and_length() {
        // Arrange
        let head_length = 0x12345678_ABCDEF90;
        // Act
        let head_and_length = HeadAndLength::from(head_length);
        // Assert
        assert_eq!(0x12345678, head_and_length.head);
        assert_eq!(0xABCDEF90, head_and_length.length);
    }

    #[test]
    fn test_half_size_in_bits() {
        // Arrange
        // Act
        let size = half_size_in_bits();
        // Assert
        assert_eq!(32, size);
    }

    #[test]
    fn test_combine_head_length() {
        // Arrange
        let head_and_length = HeadAndLength::from(0x12345678ABCDEF90);
        // Act
        let head_length = head_and_length.combine();
        // Assert
        assert_eq!(0x1234_5678_ABCD_EF90, head_length);
    }

    #[test]
    fn test_push_back() {
        // Arrange
        let mut elements: [Option<usize>; 5] = [None; 5];
        let mut queue = LFQueue::new(&mut elements);
        // Act
        assert_eq!(Ok(()), queue.push_back(1));
        assert_eq!(Ok(()), queue.push_back(2));
        assert_eq!(Ok(()), queue.push_back(3));
        assert_eq!(Ok(()), queue.push_back(4));
        assert_eq!(Ok(()), queue.push_back(5));
        assert_eq!(
            Err(AeonsErrors::MaxNumberOfActiveTasksExceeded),
            queue.push_back(6)
        );
        // Assert
        assert_eq!([Some(1), Some(2), Some(3), Some(4), Some(5)], elements);
    }

    #[test]
    fn test_pop_front() {
        // Arrange
        let mut elements: [Option<usize>; 5] = [None; 5];
        let mut queue = LFQueue::new(&mut elements);
        assert_eq!(Ok(()), queue.push_back(1));
        assert_eq!(Ok(()), queue.push_back(2));
        assert_eq!(Ok(()), queue.push_back(3));
        assert_eq!(Ok(()), queue.push_back(4));
        assert_eq!(Ok(()), queue.push_back(5));
        let head_length = queue.head_length.load(Ordering::Relaxed);
        let head_and_length = HeadAndLength::from(head_length);
        assert_eq!(0, head_and_length.head);
        assert_eq!(5, head_and_length.length);
        // Act
        assert_eq!(Some(1), queue.pop_front());
        let head_length = queue.head_length.load(Ordering::Relaxed);
        let head_and_length = HeadAndLength::from(head_length);
        assert_eq!(1, head_and_length.head);
        assert_eq!(4, head_and_length.length);

        assert_eq!(Some(2), queue.pop_front());
        let head_length = queue.head_length.load(Ordering::Relaxed);
        let head_and_length = HeadAndLength::from(head_length);
        assert_eq!(2, head_and_length.head);
        assert_eq!(3, head_and_length.length);

        assert_eq!(Some(3), queue.pop_front());
        let head_length = queue.head_length.load(Ordering::Relaxed);
        let head_and_length = HeadAndLength::from(head_length);
        assert_eq!(3, head_and_length.head);
        assert_eq!(2, head_and_length.length);

        assert_eq!(Some(4), queue.pop_front());
        let head_length = queue.head_length.load(Ordering::Relaxed);
        let head_and_length = HeadAndLength::from(head_length);
        assert_eq!(4, head_and_length.head);
        assert_eq!(1, head_and_length.length);

        assert_eq!(Some(5), queue.pop_front());
        let head_length = queue.head_length.load(Ordering::Relaxed);
        let head_and_length = HeadAndLength::from(head_length);
        assert_eq!(0, head_and_length.head);
        assert_eq!(0, head_and_length.length);

        assert_eq!(None, queue.pop_front());
        // Assert
        let head_length = queue.head_length.load(Ordering::Relaxed);
        assert_eq!(0, head_length);
        let head_and_length = HeadAndLength::from(head_length);
        assert_eq!(0, head_and_length.head);
        assert_eq!(0, head_and_length.length);
        assert_eq!([None, None, None, None, None], elements);
    }
    #[test]
    fn test_push_and_pop() {
        // Arrange
        let mut elements: [Option<usize>; 5] = [None; 5];
        let mut queue = LFQueue::new(&mut elements);
        // Act
        assert_eq!(Ok(()), queue.push_back(1));
        assert_eq!(Ok(()), queue.push_back(2));
        assert_eq!(Ok(()), queue.push_back(3));
        assert_eq!(Some(1), queue.pop_front());
        assert_eq!(Some(2), queue.pop_front());
        assert_eq!(Ok(()), queue.push_back(4));
        assert_eq!(Ok(()), queue.push_back(5));
        assert_eq!(Ok(()), queue.push_back(6));
        assert_eq!(Ok(()), queue.push_back(7));
        assert_eq!(
            Err(AeonsErrors::MaxNumberOfActiveTasksExceeded),
            queue.push_back(8)
        );
        // Assert
        assert_eq!([Some(6), Some(7), Some(3), Some(4), Some(5)], elements);
    }
}
