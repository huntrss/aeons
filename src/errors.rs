#[cfg_attr(test, derive(Debug, PartialEq))]
pub enum AeonsErrors {
    MemoryTooSmall,
    SlotIndexTooLarge,
    SlotSizeTooSmall,
    PointerIsNull,
    MaxNumberTasksExceeded,
    LockFreeStackPushError,
    MaxNumberOfActiveTasksExceeded,
}
