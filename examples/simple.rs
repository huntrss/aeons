use std::{
    future::Future,
    pin::Pin,
    task::{Context, Poll},
    time::SystemTime,
};

use aeons::{Executor, Task, TaskWaker};

// from whorl
pub struct Sleep {
    now: SystemTime,
    ms: u128,
}

// from whorl
impl Sleep {
    pub fn new(ms: u128) -> Self {
        Self {
            now: SystemTime::now(),
            ms,
        }
    }
}

impl Drop for Sleep {
    fn drop(&mut self) {
        println!("Drop for Sleep {} ms", self.ms);
        inner_drop(unsafe { Pin::new_unchecked(self)});
        fn inner_drop(this: Pin<&mut Sleep>) {
            println!("Inner drop for Sleep {} ms", this.get_mut().ms);
        }
    }
}

// from whorl
impl Future for Sleep {
    type Output = ();
    fn poll(self: Pin<&mut Self>, context: &mut Context) -> Poll<Self::Output> {
        if self.now.elapsed().unwrap().as_millis() >= self.ms {
            println!("Sleep over with {} ms", self.ms);
            Poll::Ready(())
        } else {
            context.waker().wake_by_ref();
            //context.waker().clone().wake();
            Poll::Pending
        }
    }
}

pub fn main() {
    println!("Stack allocated memory version:");
    using_stack_allocated_memory();
    println!("\nGlobal allocated memory version:");
    using_global_allocated_memory();
}

fn using_stack_allocated_memory() {
    const NUMBER_OF_TASKS: usize = 10;
    const MAXIMUM_MEMORY_SIZE_PER_TASK: usize = 64;
    let wakers: [TaskWaker; NUMBER_OF_TASKS] = aeons::default_wakers();
    let wakers = core::pin::pin!(wakers);
    let memory = [0u8; NUMBER_OF_TASKS * MAXIMUM_MEMORY_SIZE_PER_TASK];
    let memory = core::pin::pin!(memory);
    let mut futures: [Option<Task>; 10] =
        [None, None, None, None, None, None, None, None, None, None];
    let mut active_tasks: [Option<usize>; NUMBER_OF_TASKS] = [None; NUMBER_OF_TASKS];
    let mut available_tasks: [Option<usize>; NUMBER_OF_TASKS] = [None; NUMBER_OF_TASKS];
    let executor = Executor::new(
        NUMBER_OF_TASKS,
        &mut active_tasks,
        &mut available_tasks,
        MAXIMUM_MEMORY_SIZE_PER_TASK,
        memory,
        &mut futures,
        wakers,
    );
    let executor = match executor {
        Err(error) => {
            let error_code = error as u32;
            println!(
                "Executor could not be created with error code: {}",
                error_code
            );
            return;
        }
        Ok(executor) => executor,
    };
    let mut executor = std::pin::pin!(executor);
    println!("Created new runtime");
    let result = executor.spawn(Sleep::new(987));
    if result.is_err() {
        let error_code = result.unwrap_err() as u32;
        println!("Could not spawn task 1 due to error: {error_code}");
    }
    let result = executor.spawn(async {
        println!("Started execution.");
        let fut = async {
            Sleep::new(1001).await;
            5
        };
        let value = fut.await;
        println!("Awaited value {value}");
    });
    if result.is_err() {
        let error_code = result.unwrap_err() as u32;
        println!("Could not spawn task 2 due to error: {error_code}");
    }
    println!("Start running runtime");
    let result = executor.as_mut().run();
    if result.is_err() {
        let error_id = result.unwrap_err() as u32;
        println!("Run returned error {error_id}");
    }
    println!("Completed tasks:{}", executor.completed_tasks());
}

const MAX_TASKS: usize = 10;
const MAX_MEM_SIZE_TASK: usize = 64;

static mut WAKERS: [TaskWaker; MAX_TASKS] = aeons::default_wakers();
static mut MEMORY: [u8; MAX_TASKS * MAX_MEM_SIZE_TASK] = [0; MAX_TASKS * MAX_MEM_SIZE_TASK];
static mut FUTURES: [Option<Task>; MAX_TASKS] =
    [None, None, None, None, None, None, None, None, None, None];
static mut ACTIVE_TASKS: [Option<usize>; MAX_TASKS] = [None; MAX_TASKS];
static mut AVAILABLE_TASKS: [Option<usize>; MAX_TASKS] = [None; MAX_TASKS];

fn using_global_allocated_memory() {
    let pinned_wakers = unsafe { Pin::new_unchecked(&mut WAKERS) };
    let pinned_memory = unsafe { Pin::new_unchecked(&mut MEMORY) };
    let executor = Executor::new(
        MAX_TASKS,
        unsafe { &mut ACTIVE_TASKS },
        unsafe { &mut AVAILABLE_TASKS },
        MAX_MEM_SIZE_TASK,
        pinned_memory,
        unsafe { &mut FUTURES },
        pinned_wakers,
    );
    let executor = match executor {
        Err(error) => {
            let error_code = error as u32;
            println!(
                "Executor could not be created with error code: {}",
                error_code
            );
            return;
        }
        Ok(executor) => executor,
    };
    let mut executor = std::pin::pin!(executor);
    println!("Created new runtime");
    let result = executor.spawn(Sleep::new(150));
    if result.is_err() {
        let error_code = result.unwrap_err() as u32;
        println!("Could not spawn task 1 due to error: {error_code}");
    }
    let result = executor.spawn(async {
        println!("Started execution.");
        let fut = async {
            Sleep::new(200).await;
            67576
        };
        let value = fut.await;
        println!("Awaited value {value}");
    });
    if result.is_err() {
        let error_code = result.unwrap_err() as u32;
        println!("Could not spawn task 2 due to error: {error_code}");
    }
    println!("Start running runtime");
    let result = executor.as_mut().run();
    if result.is_err() {
        let error_id = result.unwrap_err() as u32;
        println!("Run returned error {error_id}");
    }
    println!("Completed tasks:{}", executor.completed_tasks());
}
